import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/login',
        component: () => import('pages/Auth.vue'),
      },
      {
        name: 'groups',
        path: '/groups',
        component: () => import('pages/Groups.vue'),
        meta: { requiresAuth: true },
      },
      {
        name: 'balance',
        path: '/balance',
        component: () => import('pages/Balance.vue'),
        meta: { requiresAuth: true },
      },
      {
        name: 'log',
        path: '/log',
        component: () => import('pages/Log.vue'),
        meta: { requiresAuth: true },
      },
      {
        name: 'profile',
        path: '/profile',
        component: () => import('pages/Profile.vue'),
        meta: { requiresAuth: true },
      },
      {
        name: 'settings',
        path: '/settings',
        component: () => import('pages/Settings.vue'),
        meta: { requiresAuth: true },
      },
      {
        name: 'home',
        path: '/',
        component: () => import('pages/Index.vue'),
        meta: { requiresAuth: true },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
