import { boot } from 'quasar/wrappers';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { firebaseConfig } from '../firebaseConfig';

//import { firestorePlugin } from 'vuefire';

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth();

export function authUser() {
  return new Promise((resolve) => {
    onAuthStateChanged(
      auth,
      (theUser) => {
        resolve(theUser);
      },
      (error) => {
        console.log(error);
      }
    );
  });
}

export default boot(async ({ app }): Promise<void> => {
  // instantiate vuefire for firestore
  //  Vue.use(firestorePlugin);
  await authUser();
  app.config.globalProperties.$db = db;
  app.config.globalProperties.$auth = auth;
});

export { db, auth };
