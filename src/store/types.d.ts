interface IGroup {
  currency: string;
  name: string;
  owner: string;
}

interface IProfile {
  uid: string;
  name: string;
  balance: number;
  groups: IGroup[];
}
