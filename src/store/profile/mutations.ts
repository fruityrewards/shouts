import { MutationTree } from 'vuex';

const mutation: MutationTree<IProfile> = {
  SET_NAME(state: IProfile, payload: string) {
    state.name = payload;
  },
  SET_UID(state: IProfile, payload: string) {
    state.uid = payload;
  },
  SET_GROUPS(state: IProfile, payload: []) {
    state.groups = payload;
  },
};

export default mutation;
