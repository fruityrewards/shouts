import { GetterTree } from 'vuex';
import { StateInterface } from '../index';

const getters: GetterTree<IProfile, StateInterface> = {
  myProfile: (theState: IProfile): IProfile | null => {
    return theState || null;
  },
};

export default getters;
