import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { auth, db } from '../../boot/firebase';
import { onAuthStateChanged, signOut } from 'firebase/auth';
import {
  collection,
  onSnapshot,
  DocumentData,
  query,
  where,
  doc,
} from 'firebase/firestore';

const actions: ActionTree<IProfile, StateInterface> = {
  async signOut() {
    await signOut(auth);
  },
  watchDB(context) {
    onAuthStateChanged(auth, (user) => {
      // Update UID
      context.commit('SET_UID', user ? user.uid : '');

      // Update Name
      context.commit('SET_NAME', user ? user.email : '');

      // Update Groups
      if (user) {
        const userDocRef = doc(db, `Users/${user.uid}`);

        const groupsRef = collection(db, 'Groups');
        const groupsCurrencyRef = query(
          groupsRef,
          where('owner', '==', userDocRef)
        );
        onSnapshot(
          groupsCurrencyRef,
          (snapshot) => {
            context.commit(
              'SET_GROUPS',
              snapshot.docs.map((doc) => {
                const data: DocumentData = doc.data();
                delete data.owner;
                return data;
              })
            );
          },
          (err) => {
            console.log(err.message);
          }
        );
      } else {
        context.commit('SET_GROUPS', []);
      }
    });
  },
};

export default actions;
