function state(): IProfile {
  return {
    uid: '',
    name: '',
    balance: 0,
    groups: [],
  };
}

export default state;
